import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RestApiModule } from './rest-api/rest-api.module';
import { CryptoapiModule } from './cryptoapi/cryptoapi.module';
import * as bodyParser from 'body-parser';



@Module({
  imports: [RestApiModule, CryptoapiModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

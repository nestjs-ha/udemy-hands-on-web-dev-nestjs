import { Controller, Get, Param, Post, Body, Put, Delete, UseFilters, BadRequestException, UsePipes, UseGuards } from '@nestjs/common';
import { CryptoCurrencyDto } from './dto/cryptoCurrency.dto';
import { UpdateCryptoCurrencyDto } from './dto/updateCryptoCurrency.dto';
import { CustomHttpExceptionFilter } from './filters/custom-http-exception.filter';
import { CryptoapiService } from './cryptoapi.service';
import { from } from 'rxjs';
import { NameValidationPipe } from './pipes/name-validation.pipe';
import { IdentifyPipe } from './pipes/identify.pipe';

@Controller('cryptoapi')
export class CryptoapiController {

    constructor(private readonly cryptoapiService: CryptoapiService ) {

    }

      // GET /cryptoapi
      @Get()
      // @Header( 'Content-Type', 'application/text')
      @UseFilters(CustomHttpExceptionFilter)
      // @UseGuards(AuthGuard('bearer'))
      fetchAll() {
         return this.cryptoapiService.fetchAll();
      }

      // GET /cyrptoapi/find/:name
      @Get('find/:name')
      @UseFilters(CustomHttpExceptionFilter)
      @UsePipes(NameValidationPipe)
      findOne(@Param('name') name: string) {
        return this.cryptoapiService.findOne(name);
      }

      @Post('add-crypto')
      @UsePipes(IdentifyPipe)
      addOne(@Body() newCrypto: CryptoCurrencyDto ) {
        return this.cryptoapiService.addOne(newCrypto);
      }

      @Put('edit-crypto')
      editOne(
          @Param('name') name: string,
          @Body() updatedCrypto: UpdateCryptoCurrencyDto,
      ) {
         return this.cryptoapiService.editOne( name, updatedCrypto );
      }

      // DELETE /cryptoapi/delete-crypto/:name
      @Delete('delete-crypto/:name')
      deleteOne(@Param('name') name: string) {
          return this.cryptoapiService.deleteOne(name);
      }
}

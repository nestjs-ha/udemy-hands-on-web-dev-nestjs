import { Module, NestModule, RequestMethod, MiddlewareConsumer } from '@nestjs/common';
import { CryptoapiService } from './cryptoapi.service';
import { CryptoapiController } from './cryptoapi.controller';
import { IdentifyMiddleware } from './middlewares/identify.middleware';

@Module({

  controllers: [ CryptoapiController],
  providers: [ CryptoapiService],
})
export class CryptoapiModule implements NestModule {
  // required method to apply middleware
  configure(consumer: MiddlewareConsumer) {
    // object with helper methods for managing middlewares
    consumer
      // which middlewares to apply (can apply list of middlare)
      .apply(IdentifyMiddleware)
      // // exclude specific requests from passing through middleware
      // .exclude(
      //   { path: 'cryptoapi', method: RequestMethod.GET },
      //   { path: 'cryptoapi', method: RequestMethod.DELETE },
      // )
      // restrict the middleware to specifc requests
      .forRoutes(CryptoapiController);
  }
}

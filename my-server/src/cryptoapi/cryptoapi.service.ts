import { Injectable } from '@nestjs/common';
import { CryptoCurrency } from './types/cryptoCurrency';
import { CryptoCurrencyDto } from './dto/cryptoCurrency.dto';
import { UpdateCryptoCurrencyDto } from './dto/updateCryptoCurrency.dto';

@Injectable()
export class CryptoapiService {
    private readonly cryptoCurrencies: Set<CryptoCurrency> = new Set([
        { name: 'bitcoin', amount: 12 },
        { name: 'ethereum', amount: 23 },
        { name: 'litecoin', amount: 34 },
        { name: 'monero', amount: 45 },
    ]);

    fetchAll() {
        return {
            msg: 'All available cryptocurrenies',
            list: [...this.cryptoCurrencies],
        };
    }

    findOne( name: string) {

        const isFound = [...this.cryptoCurrencies].some(
                crypto =>  crypto.name === name ,
        );

        return({
            msg: isFound ? 'Currency found ' : 'Currency not found',
            result : this.cryptoCurrencies,
        });
    }

    addOne(newCrypto: CryptoCurrencyDto ) {
        const isFound = [...this.cryptoCurrencies].some(
            crypto => crypto.name === newCrypto.name,
        );

        if (!isFound) {
            this.cryptoCurrencies.add(newCrypto);
            return( {
                msg: `Currency ${newCrypto.name} added`,
                list: [...this.cryptoCurrencies],
            });
        }
    }

    editOne(
       name: string,
       updatedCrypto: UpdateCryptoCurrencyDto,
    ) {
        const isFound = [...this.cryptoCurrencies].some(
            crypto => crypto.name === name,
        );

        if (isFound) {
            const updatedCryptos = [...this.cryptoCurrencies].filter(
                crypto => crypto.name !== name,
            );

            updatedCryptos.push(updatedCrypto);

            // update the cryptoCurrncies set
            this.cryptoCurrencies.clear();
            [...updatedCryptos].forEach(crypto => {
                return this.cryptoCurrencies.add( crypto );
            });
        }

        return {
            msg: `Currency ${name} updated${
                updatedCrypto.name !== name
                ? ' to ' + updatedCrypto.name : ''
            }`,
            list: [...this.cryptoCurrencies],
        };
    }

    deleteOne(name: string) {
        const i = 1;
    }


}

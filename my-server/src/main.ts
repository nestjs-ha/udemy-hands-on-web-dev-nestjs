import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // app.use(this.express.json()) // for parsing application/json
  // app.use(this.express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

  await app.listen(3000);
}
bootstrap();
